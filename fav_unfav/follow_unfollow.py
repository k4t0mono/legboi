import requests
import sqlite3
from bs4 import BeautifulSoup
#from bypass import bypass
from pprint import pprint


DB_PATH = '../fa.db'
BASE_URL = 'https://www.furaffinity.net'


def follow(user):
    url = '{}/user/{}'.format(BASE_URL, user)

    r = requests.get(url, cookies=jar)
    if r.ok != True:
        return False
    print('Following {}'.format(user), end='\t')

    soup = BeautifulSoup(r.text, 'html.parser')
    links = soup.find_all('a', { 'class':'userpage-button' })
    if len(links) < 1:
        return False

    if links[1].text == '+Watch':
        url = '{}/{}'.format(BASE_URL, links[1]['href'])
        r = requests.get(url, cookies=jar)
        if r.ok != True:
            return False
        print('Done')

    else:
        print('I got {} insted'.format(links[1].text))

    return True


if __name__ == '__main__':

    jar = requests.cookies.RequestsCookieJar()
    jar.set('__cfduid', 'd4357196652f9cf991c6756a6e71455a91555175968')
    jar.set('a', '183f5fe1-33af-43ec-aa5f-5b9659baa31e')
    jar.set('b', 'd53a7e8e-eaa7-4f0c-8811-489726c8b391')
    jar.set('s', '1')

    con = sqlite3.connect(DB_PATH)
    cursor = con.cursor()

    u = ['glitterpills', 'tltechelon', 'dovleidang', 'bambiidog', 'tempestusvulpis', 'nandowolf', 'f-r95', 'thesecretcave', 'dustyang', 'frenkyhw', 'suckmcjones', 'reizo', 'jakenwolfy', 'essenceofrapture', 'milonettle', 'boreduser', 'spelunkersal', 'dreamandnightmare', 'gorshapendragon', 'deadbeathyena']

    error = []
    sql = 'SELECT DISTINCT user FROM k4t0mono_adult;'
    for row in u:
        if not follow(row):
            print('Error')
            error.append(row)

    con.commit()
    con.close()
    print(error)
