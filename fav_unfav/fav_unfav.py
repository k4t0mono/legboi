import requests
import sqlite3
from bs4 import BeautifulSoup
#from bypass import bypass


DB_PATH = '../fa.db'
BASE_URL = 'https://www.furaffinity.net'


def fav_view(id):
    url = '{}/view/{}'.format(BASE_URL, id)
    print(url, end='\t')

    r = requests.get(url, cookies=jar)
    if r.ok != True:
        raise Exception('asd')

    soup = BeautifulSoup(r.text, 'html.parser')
    link = soup.find_all('a', {'class': 'fav'})[0]

    if link.string[0] != '-':
        print('jump', end='\t')
        return False

    fav_url = '{}{}'.format(BASE_URL, link.get('href'))

    r = requests.get(fav_url, cookies=jar)
    if r.ok != True:
        raise Exception('333')

    print('unfaved', end='\t')
    return True


if __name__ == '__main__':
    posts = []

    con = sqlite3.connect(DB_PATH)
    cursor = con.cursor()
    for row in cursor.execute('SELECT id FROM k4t0mono_adult;'):
        posts.append(int(row[0]))
    con.commit()
    con.close()

    jar = requests.cookies.RequestsCookieJar()
    jar.set('__cfduid', 'd4357196652f9cf991c6756a6e71455a91555175968')
    jar.set('a', '04328031-eb5d-4411-b601-6494fe1c3015')
    jar.set('b', '0694243a-d5be-4bdd-a844-54aa355f3a69')
    jar.set('s', '1')

    s = len(posts)
    for i in range(185, s):
        fav_view(posts[i])
        print('{:04d}/{:04d}'.format(i+1, s))
        # try:
        #cursor.execute('INSERT INTO favs VALUES(?, ?, ?)', (64010+i, posts[i], 'democelot'))
        # except sqlite3.IntegrityError:
        #     print('error on view: {}'.format(posts[i]))

        #if (i+1) % 5 == 0:
            #con.commit()
            #print('commited\t{} of {}'.format(i+1, s))


    # get_view(30974194)
