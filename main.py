import scrapy
import os
import sqlite3
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

DB_PATH = '../fa.db'
USER = 'trevu'


def get_views():
    posts = []

    con = sqlite3.connect(DB_PATH)
    c = con.cursor()
    for row in c.execute('SELECT * FROM favs WHERE post NOT IN (SELECT id FROM posts);'):
        posts.append(row[1])
    con.close()

    print(posts[:5])
    # ['30620563', '25005135', '26271573', '6128043', '5660086']
    process.crawl('view_page', posts=posts)
    process.start()


if __name__ == '__main__':
    print('Starting...')

    os.chdir('./crawler')
    s = get_project_settings()
    s.update({ 'DB_PATH': DB_PATH })
    process = CrawlerProcess(s)

    process.crawl('fav_page', user='k4t0mono')
    #process.start()
    get_views()

    os.chdir('..')

