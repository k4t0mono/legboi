import sys
import jsonlines
import numpy as np
import matplotlib.pyplot as plt
from pprint import pprint
from wordcloud import WordCloud


def calc_ratings(posts):
    rating = [ x['rating'] for x in posts ]
    rating_f = np.array(np.unique(rating, return_counts=True))

    fig, ax = plt.subplots()

    explode = [0.1, 0.0, 0.0]
    ax.pie(
        rating_f[1], labels=rating_f[0], explode=explode,
        autopct='%1.1f%%', shadow=True, startangle=90
    )
    ax.axis('equal')
    fig.suptitle('{} - Ratings'.format(user))

    plt.savefig('{}-ratings.png'.format(user))


def calc_users(posts):
    users = [ x['user'] for x in posts ]
    users_f = np.array(np.unique(users, return_counts=True)).T.tolist()
    users_f = [ (x, int(y)) for (x,y) in users_f ]
    users_f.sort(key=lambda e: e[1], reverse=True)
    users_f = users_f[:10]

    plt.rcdefaults()
    fig, ax = plt.subplots()

    y_pos = np.arange(len(users_f))
    count = [x[1] for x in users_f]
    label = [x[0] for x in users_f]

    ax.barh(
        y_pos, count, align='center'
    )
    ax.set_yticks(y_pos)
    ax.set_yticklabels(label)
    ax.invert_yaxis()
    fig.suptitle('{} - Top 10 users'.format(user))

    plt.savefig('{}-users.png'.format(user))


def calc_cat(posts):
    cat = [ x['category'] for x in posts ]
    cat_f = np.array(np.unique(cat, return_counts=True))
    explode = [0.0, 0.0, 0.0, 0.2]

    fig, ax = plt.subplots()

    ax.pie(
        cat_f[1], labels=cat_f[0], explode=explode,
        autopct='%1.1f%%', shadow=True, startangle=90
    )
    ax.axis('equal')
    fig.suptitle('{} - Categories'.format(user))

    plt.savefig('{}-categories.png'.format(user))


if __name__ == '__main__':
    user = sys.argv[1][4:-3]

    with jsonlines.open(sys.argv[1]) as reader:
        posts = [ x for x in reader]

    #calc_cat(posts)
    #calc_users(posts)
    #calc_ratings(posts)

    l = [ x['tags'] for x in posts ]
    tags = ' '.join([item for sublist in l for item in sublist])
    with open('{}-tags.txt'.format(user), 'w') as fl:
        fl.write(tags)

    users = [ x['user'] for x in posts ]
    print(users)
    with open('{}-users.txt'.format(user), 'w') as fl:
        fl.write(' '.join(users))
