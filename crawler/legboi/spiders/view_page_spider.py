# -*- coding: utf-8 -*-
import scrapy
from scrapy.exceptions import CloseSpider
from pprint import pprint
from legboi.utils.bypass import bypass
from legboi.items import Post


class ViewPageSpider(scrapy.Spider):
    name = 'view_page'

    def start_requests(self):
        posts = getattr(self, 'posts', [])
        if len(posts) < 1:
            raise CloseSpider('No post posts')

        (self.cookies, self.headers) = bypass('https://www.furaffinity.net/')
        for id_ in posts:
            url = 'https://www.furaffinity.net/view/{}/'.format(id_)

            yield scrapy.Request(url, cookies=self.cookies, headers=self.headers)

    def parse(self, response):
        post = Post()
        post['category'] = response.css('.sidebar-section-no-bottom div')[0]\
            .xpath('text()').get().split()[0]
        post['id'] = response.url.split('/')[-2]
        post['download'] = 'https:' + response.css('.download-logged-in::attr(href)').get()
        post['rating'] = response.css('.rating-box::text').get().strip()
        post['tags'] = response.css('.tags-row > span > a::text').getall()
        post['title'] = response.css('.submission-title-header::text').get()
        post['user'] = response.css('.submission-artist-container > a > h2::text').get()
        #vf = response.css('.submission-artist-stats::text')[1].get().strip()
        #post['views'] = vf[0]
        #post['favs'] = vf[2]

        yield post
