# -*- coding: utf-8 -*-
import scrapy
import logging
from scrapy.exceptions import CloseSpider
from pprint import pprint
from legboi.utils.bypass import bypass
from legboi.items import Post, Fav


class FavPageSpider(scrapy.Spider):
    name = 'fav_page'

    def start_requests(self):
        self.user = getattr(self, 'user', None)
        if self.user is None:
            raise CloseSpider('No user')

        url = 'https://www.furaffinity.net/favorites/{}'.format(self.user)
        (cookies, headers) = bypass(url)

        yield scrapy.Request(url, cookies=cookies, headers=headers)

    def parse(self, response):
        for fig in response.xpath('//figure'):
            fav = Fav()
            fav['post'] = fig.attrib['id'][4:]
            fav['fav_id'] = fig.attrib['data-fav-id']
            fav['user'] = self.user

            yield fav

        for href in response.css('a.button.right::attr(href)'):
            (cookies, headers) = bypass(response.urljoin(href.get()))
            yield response.follow(href, self.parse, cookies=cookies, headers=headers)
