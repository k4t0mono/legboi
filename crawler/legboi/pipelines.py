# -*- coding: utf-8 -*-
import sqlite3
import logging
from legboi.items import Post, Fav
from pprint import pprint


class LegboiPipeline(object):
    table = 'posts'


    def __init__(self, db_path):
        self.db_path = db_path


    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            db_path = crawler.settings.get('DB_PATH')
        )


    def open_spider(self, spider):
        self.conn = sqlite3.connect(self.db_path)
        self.cursor = self.conn.cursor()

        self.cursor.execute('''
            CREATE TABLE IF NOT EXISTS posts(
                id text NOT NULL,
                user text NOT NULL,
                title text,
                rating text,
                category text,
                views integer,
                favs integer,
                download text,

                PRIMARY KEY (id)
            );
        ''')

        self.cursor.execute('''
            CREATE TABLE IF NOT EXISTS tags(
                desc text NOT NULL UNIQUE
            );
        ''')


        self.cursor.execute('''
            CREATE TABLE IF NOT EXISTS post_tag(
                post text NOT NULL,
                tag integer NOT NULL,

                FOREIGN KEY (post) REFERENCES posts(id),
                FOREIGN KEY (tag) REFERENCES tags(rowid),
                PRIMARY KEY (post, tag)
            );
        ''')

        self.cursor.execute('''
            CREATE TABLE IF NOT EXISTS favs(
                id text NOT NULL,
                post text NOT NULL,
                user integer NOT NULL,

                UNIQUE (post, user),
                PRIMARY KEY (id),
                FOREIGN KEY (post) REFERENCES posts(id)
            );
        ''')


    def close_spider(self, spider):
        self.conn.commit()
        self.conn.close()
        self.conn = None
        self.cursor = None


    def save_tags(self, tags):
        for tag in tags:
            try:
                self.cursor.execute('INSERT INTO tags values(?);', (tag,))
            except sqlite3.IntegrityError as e:
                logging.warning('{} at: {}'.format(e, (tag,)))

        self.conn.commit()
        logging.info('tags saved')


    def save_post(self, item):
        try:
            self.cursor.execute(
                'INSERT INTO posts VALUES(?, ?, ?, ?, ?, ?, ?, ?)',
                [
                    item['id'], item['user'], item['title'], item['rating'],
                    item['category'], 0, 0, item['download']
                ]
            )
        except sqlite3.IntegrityError as e:
            logging.warning('{} at: {}'.format(e, item['id']))

        self.conn.commit()


    def save_tag_post(self, item):
        for tag in item['tags']:
            self.cursor.execute('SELECT rowid FROM tags WHERE desc=?', (tag, ))
            tag_id = tuple(self.cursor.fetchone())[0]

            try:
                self.cursor.execute('INSERT INTO post_tag VALUES(?, ?)', (item['id'], tag_id))
            except sqlite3.IntegrityError as e:
                logging.warning('{} at: {}'.format(e, (item['id'], tag_id)))

        self.conn.commit()


    def process_post(self, item, spider):
        self.save_tags(item['tags'])
        self.save_post(item)
        self.save_tag_post(item)

        return item


    def process_fav(self, item, spider):
        try:
            self.cursor.execute(
                'INSERT INTO favs VALUES(?, ?, ?)',
                (item['fav_id'], item['post'], item['user'])
            )
        except sqlite3.IntegrityError as e:
            logging.warning('{} at: {}'.format(e, item))

        self.conn.commit()

        return item


    def process_item(self, item, spider):
        if isinstance(item, Post):
            return self.process_post(item, spider)

        elif isinstance(item, Fav):
            return self.process_fav(item, spider)
