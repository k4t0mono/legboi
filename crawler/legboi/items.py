# -*- coding: utf-8 -*-
import scrapy


class Post(scrapy.Item):
    id = scrapy.Field()
    category = scrapy.Field()
    download = scrapy.Field()
    rating = scrapy.Field()
    tags = scrapy.Field()
    title = scrapy.Field()
    user = scrapy.Field()
    views = scrapy.Field()
    favs = scrapy.Field()


class Fav(scrapy.Item):
    post = scrapy.Field()
    user = scrapy.Field()
    fav_id = scrapy.Field()