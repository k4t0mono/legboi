# -*- coding: utf-8 -*-
import os
import cfscrape

FA_COOCKIE_A = os.environ['FA_COOCKIE_A']
FA_COOCKIE_B = os.environ['FA_COOCKIE_B']
USER_AGENT = 'Mozilla/5.0 (X11; U; OpenBSD i386; en-US; rv:1.8.0.5) Gecko/20060819 Firefox/1.5.0.5'


# Bypass Cloudflare DDoS protection
def bypass(url):
    (token, agent) = cfscrape.get_tokens(url, USER_AGENT)
    cookies = [
        { 'name': '__cfduid', 'value': token['__cfduid'] },
        { 'name': 'cf_clearance', 'value': token['cf_clearance'] },
        { 'name': 'a', 'value': FA_COOCKIE_A },
        { 'name': 'b', 'value': FA_COOCKIE_B },
    ]

    return (cookies, {'User-Agent': agent})